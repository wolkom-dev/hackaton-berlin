const fw = require("./serverSrc/fileWatcher");

const express = require("express");

const CONNECTIONS = [];

const ALL = [];

fw((chain)=>{
	ALL.push(...chain);
	CONNECTIONS.forEach(c => {
		try{
			c.sendUTF(JSON.stringify(ALL));
		}catch(err){
			console.log(err)
		}
	})
});

const app = express();

app.use("/", express.static("clientSrc/build"));

app.listen(80, ()=>{
	console.log("server running");
})




var WebSocketServer = require('websocket').server;
var http = require('http');
 
var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});
server.listen(8082, function() {
    console.log((new Date()) + ' Server is listening on port 8082');
});
 
wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production 
    // applications, as it defeats all standard cross-origin protection 
    // facilities built into the protocol and the browser.  You should 
    // *always* verify the connection's origin and decide whether or not 
    // to accept it. 
    autoAcceptConnections: false
});


wsServer.on('request', function(request) {
	var connection = request.accept('echo-protocol', request.origin);
    CONNECTIONS.push(connection);
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            connection.sendUTF(message.utf8Data);
        }
        else if (message.type === 'binary') {
            console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        }
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});