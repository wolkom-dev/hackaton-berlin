const fs = require("fs");

const SIZES = {};
module.exports = function(callback){
		fs.watch("./data/", (evt, filename) => {
		if(evt === "change"){
			let stats = fs.statSync("./data/"+filename);
			if(stats.size && stats.size > (SIZES[filename] || 0 ) ){
				oldSize = SIZES[filename] || 0;
				newSize = stats.size;
				SIZES[filename] = newSize;
				let read = fs.createReadStream(`./data/${filename}`, { start: oldSize, end: newSize - 1 });
				let chain;
				read.on("open", ()=>{
					chain = "";
				});
				read.on("data", (data)=>{
					chain += data;
				});
				read.on("close", ()=>{
					let lines = chain.trim().split("\n");
					lines = lines.map( l => {
						return l.split(";");
					}).filter( l => l.length === 5).map( l => {
						return {
							id : filename,
							time : parseInt(l[0]),
							temperature : parseFloat(l[1]),
							humidity : parseFloat(l[2]),
							airQuality : parseFloat(l[3]),
							contact : parseInt(l[1])
						}
					})
					callback(lines);
				});
			}
		}
	});
}