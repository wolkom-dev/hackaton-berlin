const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.jsx',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js'
  },
  devServer: {
    inline: true,
    historyApiFallback: true,
    stats: {
      colors: true,
      hash: false,
      version: false,
      chunks: false,
      children: false
    }
  },
  devtool: 'source-map',
  module: {
    loaders: [ 
      { test: /\.(js|jsx)$/, loaders: [ 'babel-loader' ], exclude: /node_modules/, include: __dirname},
      // { test: /\.css$/, loaders: ['style-loader','css-loader'] },
      { test: /\.(css)$/,  loader: `style-loader!css-loader` },
      { test: /\.(png|jpg)$/i, exclude: /node_modules/, loader: 'file' } ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.html', // Load a custom template
      inject: 'body' // Inject all scripts into the body
    })
  ]
}