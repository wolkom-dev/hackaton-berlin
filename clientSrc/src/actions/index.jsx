export function recieveData(data){
	return {
		type : "get-data",
		data : data
	}
}
var IDS = 0;

export function createOrder(order){
	return {
		type : "new-command",
		command : Object.assign({},order,{id : IDS++})
	}
}

export function completeOrder(idx){
	return {
		type : "command-completed",
		index : idx
	}
}