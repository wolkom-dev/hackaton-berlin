import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import { Chart } from "react-d3-core";
import { LineChart } from "react-d3-basic";
import Dashboard from 'react-dazzle';
import App from "./App.jsx";
import update from "react-addons-update";

import 'react-dazzle/lib/style/style.css';
import "./css/index.css";
import "antd/dist/antd.css";

let reducer = function(state = { commands : [], data : [] }, action){
  switch(action.type){
    case "get-data":
      return update(state, {
        data : { $push : action.data }
      });
    case "new-command":{
      return update(state, {
        commands : { $set : [action.command] }
      });
    }
    case "command-completed":{
      return update(state, {
        commands : { [action.index] : { completed : { $set : true}} }
      });
    }
  }
	return state;
}

const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <LocaleProvider locale={enUS}>
    <App/>
    </LocaleProvider>
  </Provider>,
  document.getElementById('mount')
)
