import React from "react";

import {connect} from "react-redux";
import randomColor from "randomcolor";
import update from "react-addons-update";
import {LineChart, XAxis, YAxis, CartesianGrid, Tooltip, Line, ResponsiveContainer} from "recharts"

export class TimeChart extends React.Component{
	
	constructor(props){
		super(props);
	}


	render(){
		const data = this.props.data;
		return <ResponsiveContainer width='100%' height={200}>
			<LineChart data={data}
				margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
				<XAxis dataKey="time" />
				<YAxis />
				<CartesianGrid strokeDasharray="3 3" />
				<Tooltip />
				<Line type="monotone" dataKey={this.props.field} stroke="#03A9F4" />
			</LineChart>
		</ResponsiveContainer>
	}
}

export default connect((state)=>{
	return {
		data : state.data
	}
})(TimeChart);