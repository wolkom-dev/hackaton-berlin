import React from "react";
import {connect} from "react-redux";

export class KpisWidget extends React.Component{

	render(){
		return <div style={{
			fontSize: "3rem",
			textAlign : "center",
			padding : "1rem" 
		}}>
			{this.props.calculate(this.props.data)}
		</div>
	}
}

export default connect(state => {
	return {
		data : state.data
	}
})(KpisWidget);