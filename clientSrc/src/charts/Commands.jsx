import React from "react";
import {Form, Input, Select, Button, Table} from "antd";
import {connect} from "react-redux";

import {createOrder, completeOrder} from "../actions/index.jsx";

const FormItem = Form.Item;
const Option = Select.Option;

export class Commands extends React.Component{
	render(){

		const {getFieldDecorator, getFieldValue} = this.props.form;
		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 6 },
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 14 },
			},
		};
		
		
		return <div style={{ width : "100%"}}>
			<Form onSubmit={(e)=>{
				e.preventDefault();
				this.props.dispatch(createOrder({
					client : getFieldValue("name"),
					articles : getFieldValue("articles")
				}));
				this.props.form.resetFields();
			}}>
				<Form.Item
					{...formItemLayout}
					label="Client"
				>
					{getFieldDecorator("name", {
						rules : [
							{required : true, message : "this fields is required"}
						]
					})(<Input/>)}
				</Form.Item>
				<Form.Item
					{...formItemLayout}
					label="Articles"
				>
					{getFieldDecorator("articles", {rules : [
							{required : true, message : "this fields is required"}
					]})(<Select
						mode="tags"
						placeholder="Please select"
						style={{ width: '100%' }}
						>
						<Option value={"1"}>Chicken</Option>
						<Option value={"2"}>Sausages</Option>
						<Option value={"3"}>Brocoli</Option>
						<Option value={"4"}>Carots</Option>
					</Select>)}
				</Form.Item>
				<FormItem style={{textAlign :"center"}}>
					<Button type="primary" htmlType="submit">Order</Button>
				</FormItem>
			</Form>
			<Table 
			columns={[{
				title : "Id",
				key : "id",
				dataIndex : "id"
			},{
				title : "Client",
				key : "client",
				dataIndex : "client"
			},{
				title : "Articles",
				key : "articles",
				dataIndex : "articles",
				render : (val, record) =>{
					return record.articles.map( k =>{
						return { "1" : "Chicken", "2" : "Sausages", "3" : "Brocoli", "4" : "Carots"}[k]
					}).join(", ");
				}
			},{
				title : "Status",
				key : "completed",
				dataIndex : "completed",
				render : (v)=>{
					return v ? <span style={{ color : "green"}}>Completed</span> : <span style={{ color : "red"}}>Ordered</span>
				}
			},{
				title : "Actions",
				key : "actions",
				dataIndex : "id",
				render : (text, record, idx)=>{
					return <Button 
						disabled={record.completed}
						onClick={()=>{
							this.props.dispatch(completeOrder(idx));
						}}
					>Complete</Button>
				}
			}]}
			dataSource={this.props.orders}/>
		</div>
	}
};

export default connect((state)=>{
	return {
		orders : state.commands
	}
})(Form.create()(Commands));