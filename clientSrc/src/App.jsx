import React, { Component } from 'react';
import Dashboard from 'react-dazzle';
import {connect} from "react-redux";
import {recieveData} from "./actions/index.jsx";

import * as websocket from "websocket";

import TimeChart from './charts/TimeChart.jsx';
import Kpis from './charts/Kpis.jsx';
import Commands from './charts/Commands.jsx';
import {message} from "antd"

// Default styles.
import 'react-dazzle/lib/style/style.css';

export class App extends Component {
  constructor(props) {
	super(props)
	this.state = {
	  widgets: {
		temperature: {
		  type: TimeChart,
		  title: 'Heat evolution',
		  props : {
			  field: "temperature",
			  title : 'Heat evolution'
		  }
		},
		commands: {
		  type: Commands,
		  title: 'Orders',
		},
		humidity: {
		  type: TimeChart,
		  title: 'Humidity evolution',
		  props : {
			  field: "humidity",
			  title : 'Humidity evolution'
		  }
		},
		airQuality: {
		  type: TimeChart,
		  title: 'Air quality evolution',
		  props : {
			  field: "airQuality",
			  title : 'Air quality evolution'
		  }
		},
		kpis: {
		  type: Kpis,
		  title: '#Sensors',
		  props: {
			  calculate : (data) => {
				  var set = new Set();
				  var sensors = 0;
				  data.forEach((k)=>{
					  if(!set.has(k.id)){
						  set.add(k.id);
						  sensors++;
					  }
					});
				return sensors;
			  }
		  }
		},
	  },
	  layout: {
		rows: [{
			columns : [
				{
				className: 'col-md-12',
					widgets: [{key: 'commands'}],
				}
			]
		},{
			columns : [
				{
				className: 'col-md-4',
				widgets: [{key: 'kpis'}],
				}
			]
		},{
		  columns: [{
				className: 'col-md-12',
				widgets: [{key: 'temperature'}],
			}
		  ],
		},{
			columns : [{
				className: 'col-md-6',
				widgets: [{key: 'humidity'}],
			}, {
				className: 'col-md-6',
				widgets: [{key: 'airQuality'}],
			}]
		}],
	  }
	};
  }

  componentDidMount(){
		console.log(websocket);
		var client = new websocket.w3cwebsocket('ws://'+window.location.hostname+':8082/', 'echo-protocol', {
			closeTimeout : 90000000
		});
		client.onerror = function() {
				message.error('Connection Error');
		};
		client.onopen = function() {
				message.success('WebSocket Client Connected');
		};
		client.onmessage = (e) => {
			this.props.dispatch(recieveData(JSON.parse(e.data).map( d => Object.assign({}, d, {time : new Date(d.time)}))));
		}
	}

  render() {
	return <div style={{
		margin : "auto",
		width : "80%"
	}}>
		<Dashboard widgets={this.state.widgets} layout={this.state.layout}  />
	</div>
  }
}

export default connect()(App);